## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

#############################
## INCLUDES FOR PRODUCTION ##
#############################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_cfg_typoscript/Configuration/TypoScript/Setup/Production" extensions="txt">

##############################
## OVERRIDE FOR DEVELOPMENT ##
##############################
[request.getNormalizedParams() && like(request.getNormalizedParams().getHttpHost(), "/development.*/")]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_cfg_typoscript/Configuration/TypoScript/Setup/Development" extensions="txt">
[END]

##########################
## INCLUDES FOR STAGING ##
##########################
[request.getNormalizedParams() && like(request.getNormalizedParams().getHttpHost(), "/staging.*/")]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_cfg_typoscript/Configuration/TypoScript/Setup/Staging" extensions="txt">
[END]