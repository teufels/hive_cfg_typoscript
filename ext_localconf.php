<?php
if(!defined('TYPO3_MODE')){
    die('Access denied.');
}

/***************
 * Make the extension configuration accessible
 */
if(!is_array($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$_EXTKEY])){
    $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$_EXTKEY] = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$_EXTKEY]);
}

/***************
 * Reset extConf array to avoid errors
 */
if(is_array($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$_EXTKEY])){
    $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$_EXTKEY] = serialize($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$_EXTKEY]);
}
